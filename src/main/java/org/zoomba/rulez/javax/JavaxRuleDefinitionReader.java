package org.zoomba.rulez.javax;

import com.esotericsoftware.yamlbeans.YamlConfig;
import com.esotericsoftware.yamlbeans.YamlReader;
import org.jeasy.rules.api.Rule;

import java.io.Reader;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;

@SuppressWarnings("unchecked")
public class JavaxRuleDefinitionReader {


    List<JavaxRuleDefinition> readAll(Reader reader) {
        List<JavaxRuleDefinition> ruleDefinitions = new ArrayList<>();
        YamlConfig yamlConfig = new YamlConfig();
        yamlConfig.readConfig.setGuessNumberTypes(true);
        YamlReader yamlReader = new YamlReader(reader, yamlConfig);
        Iterable<Object> rules ;
        try{
            Object object = yamlReader.read();
            if ( object instanceof List ) {
                rules = (List<Object>) object;
            } else if ( object instanceof Map ) {
                Object next = yamlReader.read();
                if ( next == null ) {
                    return Collections.singletonList(createRuleDefinitionFrom((Map) object));
                }
                List<Object> objects = new ArrayList<>();
                objects.add( object );
                objects.add(next);
                while ( (next = yamlReader.read())!= null ){
                    objects.add(next);
                }
                rules = objects;
            } else {
                return Collections.emptyList();
            }
        }catch (Exception e){
            if ( e instanceof RuntimeException ){ throw (RuntimeException)e; }
            throw new RuntimeException(e);
        }

        for (Object rule : rules) {
            Map<String, Object> map = (Map<String, Object>) rule;
            ruleDefinitions.add(createRuleDefinitionFrom(map));
        }
        return ruleDefinitions;
    }


    JavaxRuleDefinition read(Reader reader){
        YamlConfig yamlConfig = new YamlConfig();
        yamlConfig.readConfig.setGuessNumberTypes(true);
        YamlReader yamlReader = new YamlReader(reader, yamlConfig );
        try{
            Map<String,Object> map = (Map)yamlReader.read();
            return createRuleDefinitionFrom(map);
        }catch (Exception e){
            if ( e instanceof RuntimeException ){ throw (RuntimeException)e; }
            throw new RuntimeException(e);
        }

    }


    private static JavaxRuleDefinition createRuleDefinitionFrom(Map<String, Object> map) {
        JavaxRuleDefinition ruleDefinition = new JavaxRuleDefinition();

        String dialect = (String)map.getOrDefault("dialect", "");
        ruleDefinition.setDialect(dialect);

        String name = (String) map.get("name");
        ruleDefinition.setName(name != null ? name : Rule.DEFAULT_NAME);

        String description = (String) map.get("description");
        ruleDefinition.setDescription(description != null ? description : Rule.DEFAULT_DESCRIPTION);

        Integer priority = (Integer) map.get("priority");
        ruleDefinition.setPriority(priority != null ? priority : Rule.DEFAULT_PRIORITY);

        String condition = (String) map.get("condition");
        if (condition == null ) {
            throw new IllegalArgumentException("The rule condition must be specified");
        }
        ruleDefinition.setCondition(condition);

        List<String> actions = (List<String>) map.get("actions");
        if (actions == null || actions.isEmpty()) {
            throw new IllegalArgumentException("The rule action(s) must be specified");
        }
        ruleDefinition.setActions(actions);

        String finalAction = (String)map.getOrDefault("finally", "");
        ruleDefinition.setFinalAction(finalAction);

        return ruleDefinition;
    }
}
