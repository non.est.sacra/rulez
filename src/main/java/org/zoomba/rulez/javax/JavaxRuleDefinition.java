package org.zoomba.rulez.javax;

import org.jeasy.rules.api.Rule;

import java.util.List;

public class JavaxRuleDefinition {

    private String name = Rule.DEFAULT_NAME;
    private String description = Rule.DEFAULT_DESCRIPTION;
    private int priority = Rule.DEFAULT_PRIORITY;
    private String condition;
    private String dialect = "zm";
    private List<String> actions;
    private String finalAction = "";

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getCondition() {
        return condition;
    }

    public void setCondition(String condition) {
        this.condition = condition;
    }

    public int getPriority() {
        return priority;
    }

    public void setPriority(int priority) {
        this.priority = priority;
    }

    public List<String> getActions() {
        return actions;
    }

    public String getDialect() {
        return dialect ;
    }

    public void setDialect(String dialect){ this.dialect = dialect ; }

    public void setActions(List<String> actions) {
        this.actions = actions;
    }

    public String getFinalAction() { return finalAction; }

    public void setFinalAction(String finalAction){
        this.finalAction = finalAction;
    }

    JavaxRule create() {
        JavaxRule javaxRule = new JavaxRule()
                .name(name)
                .description(description)
                .dialect(dialect)
                .priority(priority)
                .finalAction(finalAction)
                .when(condition);
        for (String action : actions) {
            javaxRule.then(action);
        }
        return javaxRule;
    }
}