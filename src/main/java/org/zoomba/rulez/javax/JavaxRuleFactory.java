package org.zoomba.rulez.javax;

import org.jeasy.rules.api.Rule;
import org.jeasy.rules.api.Rules;

import java.io.Reader;
import java.util.List;

/**
 * Factory to create {@link JavaxRule} instances.
 *
 * @author non.est.sacra
 */
public class JavaxRuleFactory {

    private static JavaxRuleDefinitionReader reader = new JavaxRuleDefinitionReader();


    /**
     * Create a set of {@link JavaxRule} from a Reader.
     *
     * @param rulesDescriptor as a Reader
     * @return a set of rules
     */
    public static Rules createRulesFrom(Reader rulesDescriptor) {
        Rules rules = new Rules();
        List<JavaxRuleDefinition> ruleDefinition = reader.readAll(rulesDescriptor);
        for (JavaxRuleDefinition javaxRuleDefinition : ruleDefinition) {
            rules.register(javaxRuleDefinition.create());
        }
        return rules;
    }

    /**
     * Create a  {@link JavaxRule} from a Reader.
     *
     * @param rulesDescriptor as a Reader
     * @return a set of rules
     */
    public static Rule createRuleFrom(Reader rulesDescriptor) {
        JavaxRuleDefinition ruleDefinition = reader.read(rulesDescriptor);
        return ruleDefinition.create();
    }
}
