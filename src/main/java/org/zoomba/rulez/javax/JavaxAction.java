package org.zoomba.rulez.javax;

import org.jeasy.rules.api.Action;
import org.jeasy.rules.api.Facts;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.script.Compilable;
import javax.script.CompiledScript;
import javax.script.SimpleBindings;
import java.io.Serializable;

/**
 * This class is an implementation of {@link Action} that uses <a href="https://github.com/mvel/mvel">MVEL</a> to execute the action.
 *
 * @author non.est.sacra
 */
public class JavaxAction implements Action {

    private static final Logger LOGGER = LoggerFactory.getLogger(JavaxAction.class);

    private String expression;

    private CompiledScript compiledExpression;

    /**
     * Create a new {@link JavaxAction}.
     *
     * @param expression the action written in expression language
     * @param compilable a javax.Scripting.Engine instance, which supports compiling
     */
    public JavaxAction(String expression, Compilable compilable) {
        this.expression = expression;
        try {
            compiledExpression = compilable.compile(expression);
        } catch (Exception e){
            throw new RuntimeException(e);
        }
    }

    @Override
    public void execute(Facts facts) {
        try {
            compiledExpression.eval( new SimpleBindings( facts.asMap() ) );
        } catch (Exception e) {
            LOGGER.debug("Unable to evaluate expression: '" + expression + "' on facts: " + facts, e);
        }
    }
}
