package org.zoomba.rulez.javax;

import org.jeasy.rules.api.Condition;
import org.jeasy.rules.api.Facts;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.script.Compilable;
import javax.script.CompiledScript;
import javax.script.SimpleBindings;
import java.io.Serializable;

/**
 * This class is an implementation of {@link Condition} that uses <a href="https://github.com/mvel/mvel">MVEL</a> to evaluate the condition.
 *
 * @author non.est.sacra
 */
public class JavaxCondition implements Condition {

    private static final Logger LOGGER = LoggerFactory.getLogger(JavaxCondition.class);

    private String expression;
    private CompiledScript compiledExpression;

    /**
     * Create a new {@link JavaxCondition}.
     *
     * @param expression the condition written in expression language
     * @param compilable a javax.Scripting.Engine instance, which supports compiling
     */
    public JavaxCondition(String expression, Compilable compilable) {
        this.expression = expression;
        try {
            compiledExpression = compilable.compile(expression);
        }catch (Exception e){
            throw new RuntimeException(e);
        }
    }

    @Override
    public boolean evaluate(Facts facts) {
        try {
            return (boolean)compiledExpression.eval( new SimpleBindings( facts.asMap() ) );
        } catch (Exception e) {
            LOGGER.debug("Unable to evaluate expression: '" + expression + "' on facts: " + facts, e);
            return false;
        }
    }
}
