package org.zoomba.rulez.javax;

import org.jeasy.rules.api.Action;
import org.jeasy.rules.api.Condition;
import org.jeasy.rules.api.Facts;
import org.jeasy.rules.api.Rule;
import org.jeasy.rules.core.BasicRule;
import zoomba.lang.scripting.ZScriptEngine;

import javax.script.Compilable;
import javax.script.ScriptEngineManager;
import java.util.ArrayList;
import java.util.List;

/**
 * A {@link org.jeasy.rules.api.Rule} implementation that uses <a href="https://github.com/mvel/mvel">MVEL</a> to evaluate and execute the rule.
 *
 * @author non.est.sacra 
 */
public class JavaxRule extends BasicRule {

    public static class InAction implements Action{

        private InAction(){ }

        @Override
        public void execute(Facts facts) throws Exception {

        }
    }

    public static Action NOP = new InAction();

    public static final Compilable DEFAULT_ENGINE = new ZScriptEngine();
    
    private static final ScriptEngineManager SCRIPT_ENGINE_MANAGER = new ScriptEngineManager();

    private Condition condition = Condition.FALSE;

    private List<Action> actions = new ArrayList<>();

    private Action finalAction = NOP ;

    private Compilable javaxEngine;

    /**
     * Create a new Javax rule.
     */
    public JavaxRule() {
        super(Rule.DEFAULT_NAME, Rule.DEFAULT_DESCRIPTION, Rule.DEFAULT_PRIORITY);
        javaxEngine = DEFAULT_ENGINE;
    }

    /**
     * Set the dialect language, e.g.
     *    "zm" for ZoomBA, "js" for JavaScript, "rb" for Ruby "py for Python ...
     * @param javaxScriptEngineName the language
     * @return this rule
     */
    public JavaxRule dialect(String javaxScriptEngineName){
        switch ( javaxScriptEngineName ){
            case "js":
                javaxEngine = (Compilable)SCRIPT_ENGINE_MANAGER.getEngineByName("nashorn");
                break;
            case "groovy":
                javaxEngine = (Compilable)SCRIPT_ENGINE_MANAGER.getEngineByName("groovy");
                break;
            case "rb":
                javaxEngine = (Compilable)SCRIPT_ENGINE_MANAGER.getEngineByName("ruby");
                break;
            case "py":
                javaxEngine = (Compilable)SCRIPT_ENGINE_MANAGER.getEngineByName("python");
                break;
            case "zm" :
            default:
                javaxEngine = DEFAULT_ENGINE;
                break;
        }
        return this;
    }

    /**
     * Set rule name.
     *
     * @param name of the rule
     * @return this rule
     */
    public JavaxRule name(String name) {
        this.name = name;
        return this;
    }

    /**
     * Set rule description.
     *
     * @param description of the rule
     * @return this rule
     */
    public JavaxRule description(String description) {
        this.description = description;
        return this;
    }

    /**
     * Set rule priority.
     *
     * @param priority of the rule
     * @return this rule
     */
    public JavaxRule priority(int priority) {
        this.priority = priority;
        return this;
    }

    /**
     * Specify the rule's condition as MVEL expression.
     * @param condition of the rule
     * @return this rule
     */
    public JavaxRule when(String condition) {
        this.condition = new JavaxCondition(condition,javaxEngine);
        return this;
    }

    /**
     * Add an action specified as an MVEL expression to the rule.
     * @param action to add to the rule
     * @return this rule
     */
    public JavaxRule then(String action) {
        this.actions.add(new JavaxAction(action,javaxEngine));
        return this;
    }

    /**
     * Add a final action specified as an MVEL expression to the rule.
     * @param action final action to add to the rule
     * @return this rule
     */
    public JavaxRule finalAction(String action) {
        if ( action == null || action.isEmpty()) return this;
        this.finalAction = new JavaxAction(action,javaxEngine);
        return this;
    }

    @Override
    public boolean evaluate(Facts facts) {
        return condition.evaluate(facts);
    }

    @Override
    public void execute(Facts facts) throws Exception {
        for (Action action : actions) {
            action.execute(facts);
        }
    }
}
