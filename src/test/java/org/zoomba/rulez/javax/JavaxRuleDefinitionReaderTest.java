package org.zoomba.rulez.javax;

import org.jeasy.rules.api.Rule;
import org.junit.Test;

import java.io.File;
import java.io.FileReader;
import java.io.StringReader;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Collections;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;


public class JavaxRuleDefinitionReaderTest {

    private JavaxRuleDefinitionReader ruleDefinitionReader = new JavaxRuleDefinitionReader();

    @Test
    public void testRuleDefinitionReadingFromFile() throws Exception {
        // given
        File adultRuleDescriptor = new File("src/test/resources/adult-rule.yml");

        // when
        JavaxRuleDefinition adultRuleDefinition =
                ruleDefinitionReader.read(new FileReader(adultRuleDescriptor));

        // then
        assertNotNull(adultRuleDefinition);
        assertEquals("adult rule",adultRuleDefinition.getName());
        assertEquals("when age is greater then 18, then mark as adult", adultRuleDefinition.getDescription());
        assertEquals(1, adultRuleDefinition.getPriority() );
        assertEquals("person.age > 18", adultRuleDefinition.getCondition() );
        assertEquals(Collections.singletonList("person.setAdult(true);"), adultRuleDefinition.getActions());
    }

    @Test
    public void testRuleDefinitionReadingFromString() throws Exception {
        // given
        String adultRuleDescriptor = new String(Files.readAllBytes(Paths.get("src/test/resources/adult-rule.yml")));

        // when
        JavaxRuleDefinition adultRuleDefinition = ruleDefinitionReader.read(new StringReader(adultRuleDescriptor));

        // then
        assertNotNull(adultRuleDefinition);
        assertEquals("adult rule", adultRuleDefinition.getName());
        assertEquals("when age is greater then 18, then mark as adult", adultRuleDefinition.getDescription());
        assertEquals(1, adultRuleDefinition.getPriority());
        assertEquals("person.age > 18", adultRuleDefinition.getCondition());
        assertEquals(Collections.singletonList("person.setAdult(true);"), adultRuleDefinition.getActions());
    }

    @Test
    public void testRuleDefinitionReading_withDefaultValues() throws Exception {
        // given
        File adultRuleDescriptor = new File("src/test/resources/adult-rule-with-default-values.yml");

        // when
        JavaxRuleDefinition adultRuleDefinition = ruleDefinitionReader.read(new FileReader(adultRuleDescriptor));

        // then
        assertNotNull(adultRuleDefinition);
        assertEquals(Rule.DEFAULT_NAME, adultRuleDefinition.getName());
        assertEquals(Rule.DEFAULT_DESCRIPTION, adultRuleDefinition.getDescription());
        assertEquals(Rule.DEFAULT_PRIORITY, adultRuleDefinition.getPriority());
        assertEquals("person.age > 18", adultRuleDefinition.getCondition());
        assertEquals(Collections.singletonList("person.setAdult(true);"), adultRuleDefinition.getActions());
    }

    @Test(expected = IllegalArgumentException.class)
    public void testInvalidRuleDefinitionReading_whenNoCondition() throws Exception {
        // given
        File adultRuleDescriptor = new File("src/test/resources/adult-rule-without-condition.yml");

        // when
        JavaxRuleDefinition adultRuleDefinition = ruleDefinitionReader.read(new FileReader(adultRuleDescriptor));

        // then
        // expected exception
    }

    @Test(expected = IllegalArgumentException.class)
    public void testInvalidRuleDefinitionReading_whenNoActions() throws Exception {
        // given
        File adultRuleDescriptor = new File("src/test/resources/adult-rule-without-actions.yml");

        // when
        JavaxRuleDefinition adultRuleDefinition = ruleDefinitionReader.read(new FileReader(adultRuleDescriptor));

        // then
        // expected exception
    }

    @Test
    public void testRulesDefinitionReading() throws Exception {
        // given
        File rulesDescriptor = new File("src/test/resources/rules.yml");

        // when
        List<JavaxRuleDefinition> ruleDefinitions = ruleDefinitionReader.readAll(new FileReader(rulesDescriptor));

        // then
        assertEquals(2, ruleDefinitions.size());
        JavaxRuleDefinition ruleDefinition = ruleDefinitions.get(0);
        assertNotNull(ruleDefinition);
        assertEquals("adult rule", ruleDefinition.getName());
        assertEquals("when age is greater then 18, then mark as adult", ruleDefinition.getDescription());
        assertEquals(1, ruleDefinition.getPriority());
        assertEquals("person.age > 18", ruleDefinition.getCondition());
        assertEquals(Collections.singletonList("person.setAdult(true);"), ruleDefinition.getActions());

        ruleDefinition = ruleDefinitions.get(1);
        assertNotNull(ruleDefinition);
        assertEquals("weather rule", ruleDefinition.getName());
        assertEquals("when it rains, then take an umbrella", ruleDefinition.getDescription());
        assertEquals(2, ruleDefinition.getPriority());
        assertEquals("rain == true", ruleDefinition.getCondition());
        assertEquals(Collections.singletonList("System.out.println(\"It rains, take an umbrella!\");"),
                ruleDefinition.getActions());
    }

    @Test
    public void testEmptyRulesDefinitionReading() throws Exception {
        // given
        File rulesDescriptor = new File("src/test/resources/rules-empty.yml");

        // when
        List<JavaxRuleDefinition> ruleDefinitions = ruleDefinitionReader.readAll(new FileReader(rulesDescriptor));

        // then
        assertEquals(0, ruleDefinitions.size());
    }
}