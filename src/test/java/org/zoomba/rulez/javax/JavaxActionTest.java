package org.zoomba.rulez.javax;

import org.jeasy.rules.api.Action;
import org.jeasy.rules.api.Facts;
import org.junit.Test;
import zoomba.lang.core.io.ZFileSystem;
import static org.junit.Assert.assertTrue;
import java.io.PrintStream;


public class JavaxActionTest {


    @Test
    public void testJavaxActionExecution() throws Exception {
        // given
        Action markAsAdult = new JavaxAction("person.setAdult(true);", JavaxRule.DEFAULT_ENGINE );
        Facts facts = new Facts();
        Person foo = new Person("foo", 20);
        facts.put("person", foo);

        // when
        markAsAdult.execute(facts);

        // then
        assertTrue(foo.isAdult());
    }

    @Test
    public void testJavaxFunctionExecution() throws Exception {
        // given
        Action printAction = new JavaxAction("def hello() { println(\"Hello from ZoomBA!\") }; hello()",
                JavaxRule.DEFAULT_ENGINE);
        Facts facts = new Facts();
        PrintStream es = System.err;
        PrintStream os = System.out;

        System.setOut(new PrintStream("foo.txt"));
        System.setErr(new PrintStream("foo.txt"));

        // when
        printAction.execute(facts);
        System.setErr(es);
        System.setOut(os);

        // then
        assertTrue( ZFileSystem.read("foo.txt").toString().contains("Hello from ZoomBA!" ));

    }
}