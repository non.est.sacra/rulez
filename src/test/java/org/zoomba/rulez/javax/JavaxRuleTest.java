package org.zoomba.rulez.javax;

import org.jeasy.rules.api.Facts;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;


public class JavaxRuleTest {

    private Facts facts = new Facts();
    private JavaxRule javaxRule = new JavaxRule().name("rn").description("rd").priority(1);

    @Before
    public void setUp() throws Exception {
        javaxRule.when("person.age > 18");
        javaxRule.then("person.setAdult(true);");
    }

    @Test
    public void whenTheRuleIsTriggered_thenConditionShouldBeEvaluated() throws Exception {
        // given
        facts.put("person", new Person("foo", 20));

        // when
        boolean evaluationResult = javaxRule.evaluate(facts);

        // then
        assertTrue(evaluationResult);
    }

    @Test
    public void whenTheConditionIsTrue_thenActionsShouldBeExecuted() throws Exception {
        // given
        Person foo = new Person("foo", 20);
        facts.put("person", foo);

        // when
        javaxRule.execute(facts);

        // then
        assertTrue(foo.isAdult());
    }
}