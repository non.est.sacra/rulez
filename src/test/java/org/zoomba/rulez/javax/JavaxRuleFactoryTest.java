package org.zoomba.rulez.javax;

import org.jeasy.rules.api.Rule;
import org.jeasy.rules.api.Rules;
import org.junit.Test;

import java.io.File;
import java.io.FileReader;
import java.io.Reader;
import java.io.StringReader;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Iterator;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;


public class JavaxRuleFactoryTest {

    @Test
    public void testRulesCreation() throws Exception {
        // given
        File rulesDescriptor = new File("src/test/resources/rules.yml");

        // when
        Rules rules = JavaxRuleFactory.createRulesFrom(new FileReader(rulesDescriptor));

        // then
        Iterator<Rule> iterator = rules.iterator();

        Rule rule = iterator.next();
        assertNotNull(rule);
        assertEquals("adult rule", rule.getName());
        assertEquals("when age is greater then 18, then mark as adult", rule.getDescription());
        assertEquals(1, rule.getPriority());

        rule = iterator.next();
        assertNotNull(rule);
        assertEquals("weather rule", rule.getName());
        assertEquals("when it rains, then take an umbrella", rule.getDescription());
        assertEquals(2, rule.getPriority());
    }

    @Test
    public void testRuleCreationFromFileReader() throws Exception{
        // given
        Reader adultRuleDescriptorAsReader = new FileReader("src/test/resources/adult-rule.yml");

        // when
        Rule adultRule = JavaxRuleFactory.createRuleFrom(adultRuleDescriptorAsReader);

        // then
        assertEquals("adult rule", adultRule.getName());
        assertEquals("when age is greater then 18, then mark as adult", adultRule.getDescription());
        assertEquals(1, adultRule.getPriority());
    }

    @Test
    public void testRuleCreationFromStringReader() throws Exception{
        // given
        Reader adultRuleDescriptorAsReader = new StringReader(new String(Files.readAllBytes(Paths.get("src/test/resources/adult-rule.yml"))));

        // when
        Rule adultRule = JavaxRuleFactory.createRuleFrom(adultRuleDescriptorAsReader);

        // then
        assertEquals("adult rule", adultRule.getName());
        assertEquals("when age is greater then 18, then mark as adult", adultRule.getDescription());
        assertEquals(1, adultRule.getPriority());
    }
}