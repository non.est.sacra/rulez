package org.zoomba.rulez.javax;

import org.jeasy.rules.api.Condition;
import org.jeasy.rules.api.Facts;
import org.junit.Test;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.assertFalse;


public class JavaxConditionTest {

    @Test
    public void testJavaxExpressionEvaluation() throws Exception {
        // given
        Condition isAdult = new JavaxCondition("person.age > 18", JavaxRule.DEFAULT_ENGINE );
        Facts facts = new Facts();
        facts.put("person", new Person("foo", 20));

        // when
        boolean evaluationResult = isAdult.evaluate(facts);

        // then
        assertTrue(evaluationResult);
    }

    @Test
    public void whenDeclaredFactIsNotPresent_thenShouldReturnFalse() throws Exception {
        // given
        Condition isHot = new JavaxCondition("temperature > 30", JavaxRule.DEFAULT_ENGINE );
        Facts facts = new Facts();

        // when
        boolean evaluationResult = isHot.evaluate(facts);

        // then
        assertFalse(evaluationResult);
    }
}